import cors from 'cors';

const configureCors = cors({
    origin: ['http://localhost:4200', 'https://sandbox.vnpayment.vn'],
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'], // Include OPTIONS
    credentials: true,
    optionsSuccessStatus: 200
});

export default configureCors;
