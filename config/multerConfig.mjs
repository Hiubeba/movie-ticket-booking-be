import { CloudinaryStorage } from 'multer-storage-cloudinary';
import multer from 'multer';
import cloudinary from './cloudinaryConfig.mjs';

const storage = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: {
    folder: 'movie_banners', // Thư mục trên Cloudinary để lưu trữ hình ảnh
    allowedFormats: ['jpg', 'png', 'jpeg'],
  },
});

const upload = multer({ storage: storage });

export default upload;
