import passport from 'passport';
import { Strategy as GoogleStrategy } from 'passport-google-oauth20';
import { Strategy as FacebookStrategy } from 'passport-facebook';
import bcrypt from 'bcryptjs';
import User from '../models/User.mjs';

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: 'http://localhost:3000/api/auth/google/callback'
},
    async (token, tokenSecret, profile, done) => {
        try {
            console.log(profile);
            let user = await User.findOne({ googleId: profile.id });

            if (!user) {
                const defaultPassword = 'default_password';
                const hashedPassword = await bcrypt.hash(defaultPassword, 10);

                user = new User({
                    googleId: profile.id,
                    avatar: profile.photos[0].value,
                    fullName: profile.displayName,
                    email: profile.emails[0].value,
                    username: profile.emails[0].value.split('@')[0], 
                    password: hashedPassword,
                    phoneNumber: '', 
                    age: 0 
                });

                await user.save();
            }

            return done(null, user);

        } catch (err) {
            return done(err, false);
        }
    }
));

passport.use(new FacebookStrategy({
  clientID: process.env.FACEBOOK_APP_ID,
  clientSecret: process.env.FACEBOOK_APP_SECRET,
  callbackURL: 'http://localhost:3000/api/auth/facebook/callback',
  profileFields: ['id', 'first_name', 'middle_name', 'last_name', 'email']
},
async (accessToken, refreshToken, profile, done) => {
  try {
    console.log('profile: ',profile);
    let user = await User.findOne({ facebookId: profile.id });

    if (!user) {
      const { first_name, middle_name, last_name, email } = profile._json;
      const fullName = `${last_name || ''} ${middle_name || ''} ${first_name || ''}`.trim();
      console.log('=========', email);
      user = new User({
        facebookId: profile.id,
        fullName: fullName,
        email: email, 
        username: email
      });
      await user.save();
    }
    done(null, user);
  } catch (err) {
    done(err, false);
  }
}
));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    try {
        const user = await User.findById(id);
        done(null, user);
    } catch (err) {
        done(err, false);
    }
});

export default passport;
