import mongoose from 'mongoose';

const connectTestDB = async () => {
    if (mongoose.connection.readyState === 0) {
        try {
            await mongoose.connect(process.env.MONGO_TEST_URI, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
            console.log('Connected to Test MongoDB');
        } catch (err) {
            console.error('Failed to connect to Test MongoDB', err);
            process.exit(1);
        }
    }
};

const closeTestDB = async () => {
    if (mongoose.connection.readyState !== 0) {
        try {
            await mongoose.connection.close();
            console.log('Disconnected from Test MongoDB');
        } catch (err) {
            console.error('Failed to disconnect from Test MongoDB', err);
        }
    }
};

export { connectTestDB, closeTestDB };
