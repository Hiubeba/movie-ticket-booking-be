import User from "../models/User.mjs";

export const isAdmin = async (req, res, next) => {
    try {
        console.log('User ID:', req.user.id);
        const user = await User.findById(req.user.id);
        console.log('User role:', user.role);
        if(user && user.role === "admin"){
            next();
        }
        else{
            res.status(403).json({message: 'Bạn không phải là admin'});
        }
    } catch (err) {
        console.log('Error in isAdmin middleware:', err);
        res.status(500).json({message: 'Lỗi server'});
    }
}
