import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import User from '../models/User.mjs';

// Load biến môi trường từ file .env
dotenv.config();

const authMiddleware = async (req, res, next) => {
    // Lấy token từ cookie
    let token = req.cookies.token;
    console.log('Token from cookie:', token); // Debug token từ cookie

    // Nếu không có token trong cookie, thử lấy token từ header Authorization
    if (!token && req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
        token = req.headers.authorization.split(' ')[1];
        console.log('Token from header:', token); // Debug token từ header
    }

    // Nếu không tìm thấy token ở cả hai nơi, trả về lỗi 401
    if (!token) {
        return res.status(401).json({ msg: 'No token, authorization denied' });
    }

    try {
        // Giải mã token
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        // Tìm user theo decoded id và loại bỏ trường password
        req.user = await User.findById(decoded.user.id).select('-password');
        next();
    } catch (err) {
        console.error('Token verification error:', err); // Log lỗi
        res.status(401).json({ msg: 'Token is not valid' });
    }
};

export default authMiddleware;
