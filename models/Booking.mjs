import mongoose from 'mongoose';

const BookingSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    showtime: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Showtimes',
        required: true
    },
    seats: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Seat',
            required: true
        }
    ],
    totalPrice: {
        type: Number,
        required: true
    },
    paymentStatus: {
        type: String,
        enum: ['Pending', 'Completed', 'Failed'],
        default: 'Pending'
    },
    bookingDate: {
        type: Date,
        default: Date.now
    },
    transactionId: { // Thêm trường transactionId
        type: String,
        required: true,
        unique: true
    }
});

const Booking = mongoose.model('Booking', BookingSchema);
export default Booking;
