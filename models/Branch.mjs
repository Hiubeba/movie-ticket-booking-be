import mongoose from 'mongoose';

const BranchSchema = new mongoose.Schema({
     name: {
        type: String,
        required: true
     },
     country: {
        type: String,
        required: true
     },
     address: {
        type: String,
        required: true
     }
});

export default mongoose.model('Branch', BranchSchema)