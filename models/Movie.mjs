import mongoose from 'mongoose';

const MovieSchema = new mongoose.Schema({
    movieName: {
        type: String,
        required: true
    },
    banner: {
        type: String,
        required: true,
    },
    category: {
        type: String,
        required: true,
    },
    time: {
        type: Number,
        required: true,
    },
    startDay: {
        type: Date,
        required: true,
    }
})

export default mongoose.model('Movie', MovieSchema);