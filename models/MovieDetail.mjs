import mongoose from 'mongoose';

const MovieDetailSchema = new mongoose.Schema({
    movie: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Movie',
        required: true,
    },
    description: {
        type: String,
        required: true
    },
    director: {
        type: [String],
        required: true
    },
    cast: {
        type: [String],
        require: true
    },
    trailerUrl: {
        type: String,
        required: true
    },
    releaseDate: {
        type: Date,
        required: true,
    }
});

export default mongoose.model('MovieDetail', MovieDetailSchema);