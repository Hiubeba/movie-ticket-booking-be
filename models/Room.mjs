import mongoose from 'mongoose';

const RoomSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    branch: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Branch',
        required: true
    },
    seats: [
        {
            seatNumber: {
                type: String,
                required: true
            },
            row: {
                type: String,
                required: true
            },
            type: {
                type: String,
                enum: ['VIP', 'Thường', 'Đôi'],
                default: 'Thường'
            },
            isBooked: {
                type: Boolean,
                default: false
            },
            bookedBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User',
                default: null
            },
            price: {
                type: Number,
                required: true
            },
            reserved: {  // Thêm trường reserved
                type: Boolean,
                default: false
            },
            reservedUntil: {  // Thêm trường reservedUntil
                type: Date,
                default: null
            }
        }
    ]
});

const Room = mongoose.model('Room', RoomSchema);
export default Room;
