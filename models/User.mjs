import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
    googleId: {
        type: String,
        unique: true,
        sparse: true // Chỉ yêu cầu khi sử dụng Google đăng nhập
    },
    facebookId: {
        type: String,
        unique: true,
        sparse: true // Chỉ yêu cầu khi sử dụng Facebook đăng nhập
    },
    username: {
        type: String,
        unique: true,
        sparse: true
    },
    password: {
        type: String
    },
    email: {
        type: String,
        unique: true,
        sparse: true // Tạm thời không bắt buộc
    },
    phoneNumber: {
        type: String
    },
    fullName: {
        type: String,
        required: true
    },
    age: {
        type: Number
    },
    avatar: {
        type: String
    },
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    role: {
        type: String,
        enum: ['user', 'admin'],
        default: 'user'
    }
});

export default mongoose.model('User', UserSchema);
