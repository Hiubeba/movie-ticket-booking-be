import express from 'express';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import User from '../models/User.mjs';
import authMiddleware from '../middleware/authMiddleware.mjs';
import crypto from 'crypto';
import nodemailer from 'nodemailer';
import passport from 'passport';

const router = express.Router();

// Đăng ký
router.post('/register', async (req, res) => {
    const { username, password, email, phoneNumber, fullName, age } = req.body;
    console.log(req.body);
    if (!username || !password || !email || !phoneNumber || !fullName || !age) {
        return res.status(400).json({ message: 'Bạn chưa nhập đủ thông tin', data: [] });
    }

    try {
        let user = await User.findOne({ username });
        let userEmail = await User.findOne({ email });
        let userPhoneNumber = await User.findOne({ phoneNumber });

        if (user) {
            return res.status(400).json({ message: 'Username đã tồn tại', data: [] });
        }
        if (userEmail) {
            return res.status(400).json({ message: 'Email đã tồn tại', data: [] });
        }
        if (userPhoneNumber) {
            return res.status(400).json({ message: 'Số điện thoại đã tồn tại', data: [] });
        }

        user = new User({ username, password, email, phoneNumber, fullName, age });

        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);

        await user.save();

        res.status(200).json({ message: 'Thành công', data: [] });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Thất bại lỗi server', data: [] });
    }
});

// Đăng nhập
router.post('/login', async (req, res) => {
    const { username, password } = req.body;

    try {
        let user = await User.findOne({ username });
        if (!user) {
            return res.status(400).json({ message: 'Tài khoản không tồn tại', data: [] });
        }

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            return res.status(400).json({ message: 'Sai mật khẩu', data: [] });
        }

        const payload = { user: { id: user.id } };
        jwt.sign(
            payload,
            process.env.JWT_SECRET,
            { expiresIn: '1h' },
            (err, token) => {
                if (err) throw err;
                res.cookie('token', token, {
                    httpOnly: true,
                    secure: process.env.NODE_ENV === 'production', // true trong production
                    sameSite: 'none', // Quan trọng cho cross-origin
                    path: '/',
                    maxAge: 3600000 // 1 giờ
                  });
                res.status(200).json({ message: 'Thành công', token: token });
            }
        );
    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server error');
    }
});

// Logout
router.post('/logout', (req, res) => {
    res.clearCookie('token', { httpOnly: true, path: '/' });
    res.status(200).json({ message: 'Đăng xuất thành công', data: [] });
});

// Gửi yêu cầu quên mật khẩu
router.post('/forgot-password', async (req, res) => {
    const { email } = req.body;
    try {
        const user = await User.findOne({ email });
        if (!user) {
            return res.status(400).json({ message: 'Email không tồn tại', data: [] });
        }

        const token = crypto.randomBytes(20).toString('hex');
        console.log(`Generated token: ${token}`); // Debug token

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

        await user.save();

        const transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: process.env.EMAIL,
                pass: process.env.EMAIL_PASSWORD
            }
        });

        const mailOptions = {
            to: user.email,
            from: process.env.EMAIL,
            subject: 'Đặt lại mật khẩu của bạn',
            html: `
                <div style="font-family: Arial, sans-serif; max-width: 600px; margin: auto; padding: 20px; border: 1px solid #ddd; border-radius: 10px;">
                    <h2 style="color: #333;">Yêu cầu đặt lại mật khẩu</h2>
                    <p style="color: #555;">
                        Bạn nhận được email này vì bạn (hoặc ai đó) đã yêu cầu đặt lại mật khẩu tài khoản của bạn.
                    </p>
                    <p style="color: #555;">
                        Vui lòng click vào liên kết sau, hoặc dán vào trình duyệt của bạn để hoàn tất quá trình:
                    </p>
                    <p style="text-align: center;">
                        <a href="http://${req.headers.host}/reset/${token}" style="display: inline-block; padding: 10px 20px; color: #fff; background-color: #007bff; border-radius: 5px; text-decoration: none;">Đặt lại mật khẩu</a>
                    </p>
                    <p style="color: #555;">
                        Nếu bạn không yêu cầu điều này, vui lòng bỏ qua email này và mật khẩu của bạn sẽ không bị thay đổi.
                    </p>
                    <p style="color: #555;">
                        Cảm ơn bạn,
                        <br>
                        Đội ngũ hỗ trợ của chúng tôi
                    </p>
                </div>
            `
        };

        transporter.sendMail(mailOptions, (err, response) => {
            if (err) {
                console.error('Có lỗi xảy ra khi gửi email:', err);
                return res.status(500).json({ message: 'Có lỗi xảy ra khi gửi email', data: [] });
            }
            res.status(200).json({ message: 'Email đã được gửi', data: [] });
        });

    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Thất bại lỗi server', data: [] });
    }
});

// Xác nhận liên kết đặt lại mật khẩu
router.get('/reset/:token', async (req, res) => {
    try {
        const user = await User.findOne({
            resetPasswordToken: req.params.token,
            resetPasswordExpires: { $gt: Date.now() }
        });

        if (!user) {
            return res.status(400).json({
                message: 'Token không hợp lệ hoặc đã hết hạn',
                data: []
            });
        }

        res.status(200).json({ message: 'Token hợp lệ', data: [] });

    } catch (err) {
        res.status(500).json({ message: 'Thất bại lỗi server', data: [] });
    }
});

router.post('/reset/:token', async (req, res) => {
    const { password } = req.body;
    console.log(`Token received: ${req.params.token}`); // Debug token
    try {
        const user = await User.findOne({
            resetPasswordToken: req.params.token,
            resetPasswordExpires: { $gt: Date.now() }
        });

        if (!user) {
            console.log('Token không hợp lệ hoặc đã hết hạn'); // Debug message
            return res.status(400).json({ message: 'Token không hợp lệ hoặc đã hết hạn', data: [] });
        }

        console.log('User found:', user); // Debug user

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(password, salt);

        user.password = hashedPassword;
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;

        await user.save();

        console.log('Password reset successfully'); // Debug success message
        res.status(200).json({ message: 'Mật khẩu đã được đặt lại thành công', data: [] });
    } catch (err) {
        console.error('Server error:', err.message); // Detailed error message
        res.status(500).json({ message: 'Thất bại lỗi server', data: [] });
    }
});

// Route để lấy thông tin user hiện tại
router.get('/user', authMiddleware, (req, res) => {
    res.status(200).json({
        message: 'Thành công',
        data: [
            { user: req.user }
        ]
    });
});

// Route bảo vệ
router.get('/protected', authMiddleware, (req, res) => {
    res.status(200).json({ message: 'Thành công', data: [] });
});

// Đăng nhập với Google
router.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

// Callback sau khi Google xác thực người dùng
router.get('/google/callback', passport.authenticate('google', { failureRedirect: '/' }), (req, res) => {
    const payload = { user: { id: req.user.id } };
    jwt.sign(
        payload,
        process.env.JWT_SECRET,
        { expiresIn: '1h' },
        (err, token) => {
            if (err) throw err;
            res.cookie('token', token, { httpOnly: true, maxAge: 3600000 });
            res.redirect('http://localhost:4200/');
        }
    );
});

// Đăng nhập với Facebook
router.get('/facebook', passport.authenticate('facebook', { scope: ['public_profile', 'email'] }));

// Callback sau khi Facebook xác thực người dùng
router.get('/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/' }), (req, res) => {
    const payload = { user: { id: req.user.id } };
    jwt.sign(
        payload,
        process.env.JWT_SECRET,
        { expiresIn: '1h' },
        (err, token) => {
            if (err) throw err;
            res.cookie('token', token, { httpOnly: true, maxAge: 3600000 });
            res.redirect('http://localhost:4200/');
        }
    );
});

export default router;
