import express from 'express';
import Booking from '../models/Booking.mjs';
import authMiddleware from '../middleware/authMiddleware.mjs';
import { isAdmin } from '../middleware/adminMiddleware.mjs';

const router = express.Router();

router.get('/', authMiddleware, isAdmin, async(req, res)=> {
    try {
        const booking = await Booking.find();
        res.status(200).json({message: 'Thành Công', data: booking})
    } catch (error) {
        res.status(400).json({message: error.message})
    }
});

export default router;