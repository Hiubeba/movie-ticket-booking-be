import express from 'express';
import Branch from '../models/Branch.mjs';
import Room from '../models/Room.mjs';
import authMiddleware from '../middleware/authMiddleware.mjs';
import { isAdmin } from '../middleware/adminMiddleware.mjs';

const router = express.Router();

const createDefaultSeats = () => {
    const rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];
    const seatsPerRow = 16;
    let seats = [];

    rows.forEach(row => {
        for (let i = 1; i <= seatsPerRow; i++) {
            seats.push({
                seatNumber: i.toString(),
                row: row,
                type: 'Thường',
                price: 50000
            });
        }
    });

    for (let i = 1; i <= 5; i++) {
        seats.push({
            seatNumber: i.toString(),
            row: 'O',
            type: 'Đôi',
            price: 70000
        });
    }

    for (let i = 6; i <= 10; i++) {
        seats.push({
            seatNumber: i.toString(),
            row: 'A',
            type: 'VIP',
            price: 100000
        });
    }

    return seats;
}

// Thêm chi nhánh rạp
router.post('/', authMiddleware, isAdmin, async(req, res) => {
    try {
        const newBranch = new Branch(req.body);
        const saveBranch = await newBranch.save();
        res.status(200).json({ message: 'Thành Công', data: saveBranch });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

// Sửa chi nhánh rạp
router.put('/:id', authMiddleware, isAdmin, async(req, res) => {
    try {
        const updateBranch = await Branch.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.status(200).json({ message: 'Thành công', data: updateBranch });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

// Xóa rạp
router.delete('/:id', authMiddleware, isAdmin, async(req, res) => {
    try {
        const deleteBranch = await Branch.findByIdAndDelete(req.params.id);
        res.status(200).json({ message: 'Đã xóa rạp phim' });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

// Tìm kiếm rạp phim
router.get('/:id', async(req, res) => {
    try {
        const branch = await Branch.findById(req.params.id).populate('rooms');
        if (!branch) {
            return res.status(404).json({ message: 'Không tồn tại rạp' });
        }
        res.status(200).json({ message: 'Thành Công', data: branch });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

// Lấy ra toàn bộ rạp phim
router.get('/', async(req, res) => {
    try {
        const branches = await Branch.find().populate('rooms');
        res.status(200).json({ message: 'Thành công', data: branches });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

// Tạo phòng chiếu mới cho chi nhánh
router.post('/:branchId/rooms', authMiddleware, isAdmin, async(req, res) => {
    const room = new Room({
        ...req.body,
        branch: req.params.branchId,
        seats: createDefaultSeats() // Sử dụng hàm để tạo ghế mặc định
    });
    try {
        const savedRoom = await room.save();
        await Branch.findByIdAndUpdate(req.params.branchId, { $push: { rooms: savedRoom._id } });
        res.status(201).json({ message: 'Thành Công', data: savedRoom });
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Lấy danh sách các phòng chiếu cho chi nhánh
router.get('/:branchId/rooms', async(req, res) => {
    try {
        const rooms = await Room.find({ branch: req.params.branchId });
        res.status(200).json({ message: 'Thành Công', data: rooms });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

export default router;
