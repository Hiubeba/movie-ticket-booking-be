import express from 'express';
import Movie from '../models/Movie.mjs';
import { isAdmin } from '../middleware/adminMiddleware.mjs';
import authMiddleware from '../middleware/authMiddleware.mjs';
import upload from '../config/multerConfig.mjs';
const router = express.Router();

router.post('/', authMiddleware, isAdmin, upload.single('banner') , async(req, res) => {

    const file = req.file;

    if(!file){
        return res.status(400).json({message: 'No file uploaded'});
    }

    try {
        const newMovie = new Movie(
            {
                ...req.body,
                banner: file.path
            }
        );
        const savedMovie = await newMovie.save();
        res.status(201).json({message: 'Thành công', data: savedMovie});
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

router.put('/:id', authMiddleware, isAdmin, upload.single('banner'), async(req, res) => {
    const file = req.file;

    try {
        let updateData = req.body;

        if(file){
            updateData.banner = file.path;
        }

        const updatedMovie = await Movie.findByIdAndUpdate(req.params.id, updateData, { new: true });
        res.status(200).json({message: 'Thành công', data: updatedMovie});
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

router.delete('/:id', authMiddleware, isAdmin, async (req, res) => {
    try {
        await Movie.findByIdAndDelete(req.params.id);
        res.status(200).json({message: 'Xóa thành công'});
    } catch (err) {
        res.status(400).json({message: err.message});
    }
});

router.get('/', async(req, res) => {
    try {
        const movies = await Movie.find();
        res.status(200).json({message: 'Thành công', data: movies});
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

export default router;
