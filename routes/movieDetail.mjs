import express from 'express';
import MovieDetail from '../models/MovieDetail.mjs';
import authMiddleware from '../middleware/authMiddleware.mjs';
import { isAdmin } from '../middleware/adminMiddleware.mjs';

const router = express.Router();

// Thêm chi tiết phim
router.post('/', authMiddleware, isAdmin, async(req, res) => {
    try {
        const newDetail = new MovieDetail(req.body);
        const savedDetail = await newDetail.save();
        res.status(200).json({message: 'Thành công', data: savedDetail});
    } catch (error) {
        res.status(400).json({message: error.message})
    }
});

// Cập nhật chi tiết phim
router.put('/:id', authMiddleware, isAdmin, async (req, res) => {
    try {
        const updateDetail = await MovieDetail.findByIdAndUpdate(req.params.id, req.body, {new: true});
        res.status(200).json({message: 'Thành công', data: updateDetail});
    } catch (error) {
        res.status(400).json({message: error.message})
    }
});

//Xóa chi tiết phim
router.delete('/:id', authMiddleware, isAdmin, async(req, res)=> {
    try {
        const deleteDetail = await MovieDetail.findByIdAndDelete(req.params.id);
        res.status(200).json({message: 'Thành công'});
    } catch (error) {
        res.status(400).json({message: error.message});
    }
})

//Lấy chi tiết của phim theo id
router.get('/:id', async (req, res) => {
    try {
        const detail = await MovieDetail.findOne({ movie: req.params.id }).populate('movie');
        if (!detail) {
            return res.status(404).json({ message: 'Chi tiết bộ phim không tồn tại' });
        }
        res.status(200).json({ message: 'Thành công', data: detail });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});


router.get('/', async(req, res)=> {
    try {
        const movieDetail = await MovieDetail.find();
        res.status(200).json({message: 'Thành Công', data: movieDetail})
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})


export default router;