import express from 'express';
import moment from 'moment';
import dotenv from 'dotenv';
import querystring from 'qs';
import crypto from 'crypto';
import config from 'config';
import authMiddleware from '../middleware/authMiddleware.mjs';
import Showtimes from '../models/Showtimes.mjs';
import Room from '../models/Room.mjs';
import Booking from '../models/Booking.mjs';

const router = express.Router();
dotenv.config();

// Hàm để sắp xếp đối tượng
function sortObject(obj) {
    let sorted = {};
    let str = [];
    let key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            str.push(encodeURIComponent(key));
        }
    }
    str.sort();
    for (key = 0; key < str.length; key++) {
        sorted[str[key]] = encodeURIComponent(obj[str[key]]).replace(/%20/g, "+");
    }
    return sorted;
}

router.post('/select_seat', authMiddleware, async (req, res) => {
    try {
        const { showtimeId, seatId } = req.body;
        const userId = req.user._id;

        const showtime = await Showtimes.findById(showtimeId).populate('room');
        if (!showtime) {
            return res.status(404).json({ message: 'Không tìm thấy lịch chiếu' });
        }

        const room = await Room.findById(showtime.room._id);
        if (!room) {
            return res.status(404).json({ message: 'Không tìm thấy phòng chiếu' });
        }

        const seat = room.seats.id(seatId);
        if (!seat) {
            return res.status(404).json({ message: `Ghế ${seatId} không tồn tại` });
        }

        if (seat.isBooked || seat.reserved) {
            return res.status(400).json({ message: `Ghế ${seatId} đã được đặt hoặc đang giữ chỗ` });
        }

        seat.reserved = true;
        seat.reservedUntil = new Date(Date.now() + 2 * 60 * 1000); // Giữ chỗ trong 2 phút
        seat.bookedBy = userId;

        await room.save();

        // Phát sóng thông báo cho tất cả client về ghế đã giữ chỗ
        req.io.emit('seatSelected', {
            roomId: room._id,
            seatId: seatId,
            userId: userId
        });

        res.status(200).json({ message: 'Ghế đã được chọn' });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.post('/create_payment_url', authMiddleware, async (req, res) => {
    try {
        const showtimeId = req.body.showtimeId;
        const selectedSeats = req.body.selectedSeats;
        const userId = req.user._id;

        const showtime = await Showtimes.findById(showtimeId).populate('room');
        if (!showtime) {
            return res.status(404).json({ message: 'Không tìm thấy lịch chiếu' });
        }

        const room = await Room.findById(showtime.room._id);
        if (!room) {
            return res.status(404).json({ message: 'Không tìm thấy phòng chiếu' });
        }

        let totalPrice = 0;
        const bookedSeats = [];
        const now = new Date();

        for (const seatId of selectedSeats) {
            const seat = room.seats.id(seatId);
            if (!seat) {
                return res.status(404).json({ message: `Ghế ${seatId} không tồn tại` });
            }

            if (seat.isBooked || (seat.reserved && seat.reservedUntil > now)) {
                return res.status(400).json({ message: `Ghế ${seatId} đã được đặt hoặc đang giữ chỗ` });
            }

            seat.isBooked = true;
            seat.reserved = false;
            seat.reservedUntil = null;
            seat.bookedBy = userId;
            totalPrice += seat.price;
            bookedSeats.push(seat._id);
        }

        await room.save();

        // Thông báo cho tất cả client về ghế đã giữ chỗ
        req.io.emit('seatReserved', {
            roomId: room._id,
            seats: selectedSeats
        });

        // Create a booking record
        const orderId = moment().format('DDHHmmss'); // Tạo mã giao dịch
        const newBooking = new Booking({
            user: userId,
            showtime: showtimeId,
            seats: bookedSeats,
            totalPrice,
            paymentStatus: 'Pending',
            transactionId: orderId
        });

        await newBooking.save();

        process.env.TZ = 'Asia/Ho_Chi_Minh';

        let date = new Date();
        let createDate = moment(date).format('YYYYMMDDHHmmss');

        let ipAddr = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;

        let tmnCode = config.get('vnp_TmnCode');
        let secretKey = config.get('vnp_HashSecret');
        let vnpUrl = config.get('vnp_Url');
        let returnUrl = config.get('vnp_ReturnUrl');
        let amount = req.body.amount;
        let bankCode = req.body.bankCode;

        let locale = req.body.language;
        if (locale === null || locale === '') {
            locale = 'vn';
        }
        let currCode = 'VND';
        let vnp_Params = {};
        vnp_Params['vnp_Version'] = '2.1.0';
        vnp_Params['vnp_Command'] = 'pay';
        vnp_Params['vnp_TmnCode'] = tmnCode;
        vnp_Params['vnp_Locale'] = locale;
        vnp_Params['vnp_CurrCode'] = currCode;
        vnp_Params['vnp_TxnRef'] = orderId;
        vnp_Params['vnp_OrderInfo'] = 'Thanh toan cho ma GD:' + orderId;
        vnp_Params['vnp_OrderType'] = 'other';
        vnp_Params['vnp_Amount'] = amount * 100;
        vnp_Params['vnp_ReturnUrl'] = returnUrl;
        vnp_Params['vnp_IpAddr'] = ipAddr;
        vnp_Params['vnp_CreateDate'] = createDate;
        if (bankCode !== null && bankCode !== '') {
            vnp_Params['vnp_BankCode'] = bankCode;
        }

        vnp_Params = sortObject(vnp_Params);

        let signData = querystring.stringify(vnp_Params, { encode: false });
        let hmac = crypto.createHmac("sha512", secretKey);
        let signed = hmac.update(Buffer.from(signData, 'utf-8')).digest("hex");
        vnp_Params['vnp_SecureHash'] = signed;
        vnpUrl += '?' + querystring.stringify(vnp_Params, { encode: false });

        // Trả về URL thanh toán thay vì chuyển hướng
        res.json({ paymentUrl: vnpUrl });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/vnpay_return', async (req, res) => {
    try {
        let vnp_Params = req.query;
        const secureHash = vnp_Params['vnp_SecureHash'];

        delete vnp_Params['vnp_SecureHash'];
        delete vnp_Params['vnp_SecureHashType'];

        vnp_Params = sortObject(vnp_Params);

        const vnp_HashSecret = config.get('vnp_HashSecret');
        const signData = querystring.stringify(vnp_Params, { encode: false });
        const hmac = crypto.createHmac("sha512", vnp_HashSecret);
        const signed = hmac.update(Buffer.from(signData, 'utf-8')).digest("hex");

        if (secureHash === signed) {
            const orderId = vnp_Params['vnp_TxnRef'];
            const rspCode = vnp_Params['vnp_ResponseCode'];

            const booking = await Booking.findOne({ transactionId: orderId });
            if (!booking) {
                return res.status(404).json({ message: 'Booking not found' });
            }

            booking.paymentStatus = rspCode === '00' ? 'Completed' : 'Failed'; // Sửa 'Paid' thành 'Completed'
            await booking.save();

            res.status(200).json({ code: rspCode, message: 'Giao dịch thành công', data: booking });
        } else {
            res.status(200).json({ code: '97', message: 'Chữ ký không hợp lệ' });
        }
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

router.get('/vnpay_ipn', async (req, res) => {
    try {
        let vnp_Params = req.query;
        const secureHash = vnp_Params['vnp_SecureHash'];

        delete vnp_Params['vnp_SecureHash'];
        delete vnp_Params['vnp_SecureHashType'];

        vnp_Params = sortObject(vnp_Params);

        const vnp_HashSecret = config.get('vnp_HashSecret');
        const signData = querystring.stringify(vnp_Params, { encode: false });
        const hmac = crypto.createHmac("sha512", vnp_HashSecret);
        const signed = hmac.update(Buffer.from(signData, 'utf-8')).digest("hex");

        if (secureHash === signed) {
            const orderId = vnp_Params['vnp_TxnRef'];
            const rspCode = vnp_Params['vnp_ResponseCode'];

            const booking = await Booking.findOne({ transactionId: orderId });
            if (!booking) {
                return res.status(200).json({ RspCode: '01', Message: 'Order not found' });
            }

            if (booking.paymentStatus !== 'Pending') {
                return res.status(200).json({ RspCode: '02', Message: 'Order already confirmed' });
            }

            booking.paymentStatus = rspCode === '00' ? 'Completed' : 'Failed'; // Sửa 'Paid' thành 'Completed'
            await booking.save();

            res.status(200).json({ RspCode: '00', Message: 'Confirm Success' });
        } else {
            res.status(200).json({ RspCode: '97', Message: 'Invalid Signature' });
        }
    } catch (error) {
        res.status(200).json({ RspCode: '99', Message: 'Unknown error' });
    }
});

export default router;
