import express from 'express';
import Room from '../models/Room.mjs';
import Showtimes from '../models/Showtimes.mjs';
import Booking from '../models/Booking.mjs'; // Import model Booking
import authMiddleware from '../middleware/authMiddleware.mjs';

const router = express.Router();

// Lấy thông tin ghế cho 1 xuất chiếu cụ thể và rạp cụ thể 
router.get('/:showtimeId/seats', async (req, res) => {
    try {
        const showtimeId = req.params.showtimeId;
        const showtime = await Showtimes.findById(showtimeId).populate('room');
        if (!showtime) {
            return res.status(404).json({ message: 'Không tìm thấy lịch chiếu' });
        }

        const room = await Room.findById(showtime.room._id);
        if (!room) {
            return res.status(404).json({ message: 'Không tìm thấy phòng chiếu' });
        }

        res.json(room);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Đặt ghế 
router.post('/:showtimeId/seats/book', authMiddleware, async (req, res) => {
    try {
        const { showtimeId } = req.params;
        const { selectedSeats } = req.body; // Expecting an array of seat IDs from client
        const userId = req.user._id; // Lấy thông tin người dùng từ middleware

        const showtime = await Showtimes.findById(showtimeId).populate('room');
        if (!showtime) {
            return res.status(404).json({ message: 'Không tìm thấy lịch chiếu' });
        }

        const room = await Room.findById(showtime.room._id);
        if (!room) {
            return res.status(404).json({ message: 'Không tìm thấy phòng chiếu' });
        }

        let totalPrice = 0;
        const bookedSeats = [];

        for (const seatId of selectedSeats) {
            const seat = room.seats.id(seatId);
            if (!seat) {
                return res.status(404).json({ message: `Ghế ${seatId} không tồn tại` });
            }

            if (seat.isBooked) {
                return res.status(400).json({ message: `Ghế ${seatId} đã được đặt` });
            }

            seat.isBooked = true;
            seat.bookedBy = userId;
            totalPrice += seat.price;
            bookedSeats.push(seat._id);
        }

        await room.save();

        // Create a booking record
        const newBooking = new Booking({
            user: userId,
            showtime: showtimeId,
            seats: bookedSeats,
            totalPrice,
            paymentStatus: 'Pending'
        });

        await newBooking.save();

        res.status(200).json({ message: 'Đặt ghế thành công', booking: newBooking });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

export default router;
