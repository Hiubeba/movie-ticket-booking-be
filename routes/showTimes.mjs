import express from 'express';
import Showtimes from '../models/Showtimes.mjs';
import authMiddleware from '../middleware/authMiddleware.mjs';
import { isAdmin } from '../middleware/adminMiddleware.mjs';

const router = express.Router();

router.post('/', authMiddleware, isAdmin, async(req, res) => {
    try {
        const newShowtime = new Showtimes(req.body);
        const saveShowtime = await newShowtime.save(); 

        res.status(200).json({ message: 'Thành Công', data: saveShowtime });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.put('/:id', authMiddleware, isAdmin, async(req, res) => {
    try {
        const updateShowtime = await Showtimes.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.status(200).json({ message: 'Thành công', data: updateShowtime });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.delete('/:id', authMiddleware, isAdmin, async(req, res) => {
    try {
        const deleteShowtime = await Showtimes.findByIdAndDelete(req.params.id);
        res.status(200).json({ message: 'Xóa lịch chiếu thành công', data: deleteShowtime });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/:id', async (req, res) => {
    try {
        const movieId = req.params.id;

        // Lấy tất cả lịch chiếu liên quan đến movieId
        const allShowtimesForMovie = await Showtimes.find({ movie: movieId })
            .populate('movie')
            .populate('branch')
            .populate('room');

        if (!allShowtimesForMovie || allShowtimesForMovie.length === 0) {
            return res.status(404).json({ message: 'Không tìm thấy lịch chiếu' });
        }

        res.status(200).json({ message: 'Thành Công', data: allShowtimesForMovie });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/:id/showtimes', async (req, res) => {
    try {
        const showtimeId = req.params.id;

        // Lấy tất cả lịch chiếu liên quan đến movieId
        const allShowtimesForMovie = await Showtimes.findById(showtimeId)
            .populate('movie')
            .populate('branch')
            .populate('room');

        if (!allShowtimesForMovie || allShowtimesForMovie.length === 0) {
            return res.status(404).json({ message: 'Không tìm thấy lịch chiếu' });
        }

        res.status(200).json({ message: 'Thành Công', data: allShowtimesForMovie });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/:movieId/:branchId', async (req, res) => {
    try {
        const movieId = req.params.movieId;
        const branchId = req.params.branchId;

        const getShowtime = await Showtimes.findOne({
            movie: movieId,
            branch: branchId
        }).populate('movie').populate('branch').populate('room');

        if (!getShowtime) {
            return res.status(404).json({ message: 'Không tìm thấy lịch chiếu' });
        }

        res.status(200).json({ message: 'Thành Công', data: getShowtime });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/', async(req, res) => {
    try {
        const showtimes = await Showtimes.find().populate('movie').populate('branch').populate('room');
        res.status(200).json({ message: 'Thành Công', data: showtimes });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

export default router;
