import Room from '../models/Room.mjs';

export async function releaseExpiredSeats() {
    const now = new Date();
    const rooms = await Room.find({
        'seats.reserved': true,
        'seats.reservedUntil': { $lte: now }
    });

    for (const room of rooms) {
        let updated = false;
        for (const seat of room.seats) {
            if (seat.reserved && seat.reservedUntil <= now) {
                seat.reserved = false;
                seat.reservedUntil = null;
                seat.bookedBy = null;
                updated = true;
            }
        }
        if (updated) {
            await room.save();
        }
    }
}
