import mongoose from 'mongoose';
import dotenv from 'dotenv';
import Branch from '../models/Branch.mjs';
import Room from '../models/Room.mjs';

// Load biến môi trường từ file .env
dotenv.config();

// Kết nối đến MongoDB
const connectDB = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    console.log('Connected to MongoDB');
  } catch (err) {
    console.log('Failed to connect to MongoDB', err);
    process.exit(1);
  }
};

const createBranchAndSeats = async () => {
  try {
    await connectDB();

    // Tạo chi nhánh
    const branch = new Branch({
      name: 'Galaxy Nguyen Du',
      country: 'Vietnam',
      address: '20 Nguyen Du, Ho Chi Minh'
    });

    const savedBranch = await branch.save();

    // Tạo phòng chiếu
    const room = new Room({
      name: 'Rạp 1',
      branch: savedBranch._id
    });

    const rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'];
    const numberOfSeatsPerRow = 20;
    const seats = [];

    rows.forEach(row => {
      for (let i = 1; i <= numberOfSeatsPerRow; i++) {
        let seatType = 'Thường';
        if (row === 'P' || row === 'O') {
          seatType = 'VIP';
        } else if ((row === 'A' && (i === 9 || i === 10)) || (row === 'B' && (i === 9 || i === 10))) {
          seatType = 'Đôi';
        }
        seats.push({
          seatNumber: `${row}${i}`,
          row: row,
          type: seatType
        });
      }
    });

    room.seats = seats;
    const savedRoom = await room.save();

    // Cập nhật chi nhánh với phòng chiếu mới
    savedBranch.rooms = [savedRoom._id];
    await savedBranch.save();

    console.log('Branch and seats created successfully');
    await mongoose.disconnect();
  } catch (error) {
    console.error('Error creating branch and seats:', error);
    await mongoose.disconnect();
  }
};

createBranchAndSeats();
