// server.mjs
import express from 'express';
import http from 'http'; // Import thêm thư viện http
import { Server } from 'socket.io'; // Import thư viện socket.io
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';

import authRoutes from './routes/auth.mjs';
import movieRoutes from './routes/movie.mjs';
import branch from './routes/branch.mjs';
import showtimes from './routes/showTimes.mjs';
import movieDetailRouters from './routes/movieDetail.mjs';
import seat from './routes/seat.mjs';
import booking from './routes/booking.mjs';
import order from './routes/order.mjs';

import connectDB from './config/db.mjs';
import configureCors from './config/corsConfig.mjs';
import dotenv from 'dotenv';
import passport from './config/passportConfig.mjs';

import { releaseExpiredSeats } from './scripts/releaseSeats.mjs'; // Import hàm releaseExpiredSeats

// Load biến môi trường từ file .env
dotenv.config();

// Kết nối đến MongoDB
connectDB();

const app = express();
const server = http.createServer(app); // Tạo HTTP server
const io = new Server(server, {
    cors: {
        origin: "http://localhost:4200", // Thay đổi theo URL của ứng dụng frontend của bạn
        methods: ["GET", "POST"]
    }
});

// Middleware
app.use(configureCors); // Đặt middleware CORS đầu tiên
app.use(bodyParser.json());
app.use(cookieParser());

// Cấu hình express-session
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: { secure: false }
}));

app.use(passport.initialize());
app.use(passport.session());

// Thêm middleware để chia sẻ đối tượng io với các route
app.use((req, res, next) => {
    req.io = io;
    next();
});

// Routes
app.use('/api/auth', authRoutes);
app.use('/api/admin', movieRoutes);
app.use('/api/admin/movieDetail', movieDetailRouters);
app.use('/api/admin/branch', branch);
app.use('/api/admin/showtimes', showtimes);

app.use('/api/seat', seat);
app.use('/api/booking', booking);
app.use('/api/order', order);

const PORT = process.env.PORT || 5000;
server.listen(PORT, '0.0.0.0', () => console.log(`Server running on port ${PORT}`));

// Chạy hàm mỗi phút
setInterval(releaseExpiredSeats, 60000);

// Socket.io logic
io.on('connection', (socket) => {
    console.log('New client connected');

    socket.on('seatSelected', (data) => {
        io.emit('seatSelected', data);
    });
    
    socket.on('disconnect', () => {
        console.log('Client disconnected');
    });
});

export default app;
