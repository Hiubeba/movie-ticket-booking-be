import chai from 'chai';
import chaiHttp from 'chai-http';
import sinon from 'sinon';
import path from 'path';
import { fileURLToPath } from 'url';
import dotenv from 'dotenv';
import passport from 'passport';
import server from '../server.mjs';
import User from '../models/User.mjs';
import { connectTestDB, closeTestDB } from '../config/testDB.js';

dotenv.config();

chai.use(chaiHttp);
const should = chai.should();

// Định nghĩa __dirname
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

describe('Auth API', function () {
    this.timeout(10000); // 10 seconds

    before(async () => {
        await connectTestDB();
        await User.deleteMany({});
    });

    after(async () => {
        await closeTestDB();
    });

    describe('/POST register', () => {
        it('it should register a new user', (done) => {
            const user = {
                username: 'testuser',
                password: '123456',
                email: 'nth.caubegaxoac@gmail.com',
                phoneNumber: '0935258701',
                fullName: 'Nguyễn Trung Hiếu',
                age: 18
            };
            chai.request(server)
                .post('/api/auth/register')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Thành công');
                    res.body.should.have.property('data').eql([]);
                    done();
                });
        });
    });

    describe('/POST login', () => {
        it('it should login a user', (done) => {
            const user = {
                username: 'testuser',
                password: '123456'
            };
            chai.request(server)
                .post('/api/auth/login')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Thành công');
                    res.body.should.have.property('data').eql([]);
                    res.should.have.cookie('token');
                    done();
                });
        });
    });

    describe('/POST logout', () => {
        let token;

        before((done) => {
            const user = {
                username: 'testuser',
                password: '123456'
            };
            chai.request(server)
                .post('/api/auth/login')
                .send(user)
                .end((err, res) => {
                    token = res.headers['set-cookie'][0].split(';')[0].split('=')[1];
                    done();
                });
        });

        it('it should logout a user', (done) => {
            chai.request(server)
                .post('/api/auth/logout')
                .set('Cookie', `token=${token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Đăng xuất thành công');
                    res.body.should.have.property('data').eql([]);
                    done();
                });
        });
    });

    describe('/GET user', () => {
        let token;

        before((done) => {
            const user = {
                username: 'testuser',
                password: '123456'
            };
            chai.request(server)
                .post('/api/auth/login')
                .send(user)
                .end((err, res) => {
                    token = res.headers['set-cookie'][0].split(';')[0].split('=')[1];
                    done();
                });
        });

        it('it should get the current user', (done) => {
            chai.request(server)
                .get('/api/auth/user')
                .set('Cookie', `token=${token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Thành công');
                    res.body.data[0].user.should.have.property('username').eql('testuser');
                    res.body.data[0].user.should.have.property('email').eql('nth.caubegaxoac@gmail.com');
                    res.body.data[0].user.should.have.property('phoneNumber').eql('0935258701');
                    res.body.data[0].user.should.have.property('fullName').eql('Nguyễn Trung Hiếu');
                    res.body.data[0].user.should.have.property('age').eql(18);
                    done();
                });
        });
    });

    describe('/GET protected', () => {
        let token;

        before((done) => {
            const user = {
                username: 'testuser',
                password: '123456'
            };
            chai.request(server)
                .post('/api/auth/login')
                .send(user)
                .end((err, res) => {
                    token = res.headers['set-cookie'][0].split(';')[0].split('=')[1];
                    done();
                });
        });

        it('it should access protected route', (done) => {
            chai.request(server)
                .get('/api/auth/protected')
                .set('Cookie', `token=${token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Thành công');
                    res.body.should.have.property('data').eql([]);
                    done();
                });
        });
    });

    describe('/POST forgot-password', function () {
        this.timeout(10000);

        it('it should send a reset password email', (done) => {
            const email = { email: 'nth.caubegaxoac@gmail.com' };
            chai.request(server)
                .post('/api/auth/forgot-password')
                .send(email)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Email đã được gửi');
                    res.body.should.have.property('data').eql([]);
                    done();
                });
        });
    });

    describe('/POST reset/:token', function () {
        this.timeout(10000);

        let resetToken;

        before((done) => {
            const email = { email: 'nth.caubegaxoac@gmail.com' };
            chai.request(server)
                .post('/api/auth/forgot-password')
                .send(email)
                .end((err, res) => {
                    if (err) {
                        done(err);
                    } else {
                        User.findOne({ email: 'nth.caubegaxoac@gmail.com' }).then(user => {
                            resetToken = user.resetPasswordToken;
                            console.log(`Generated reset token in test: ${resetToken}`);
                            done();
                        }).catch(done);
                    }
                });
        });

        it('it should reset the password', (done) => {
            const newPassword = { password: 'newpassword123' };
            chai.request(server)
                .post(`/api/auth/reset/${resetToken}`)
                .send(newPassword)
                .end((err, res) => {
                    if (err) {
                        console.log(err);
                        done(err);
                    } else {
                        res.should.have.status(200);
                        res.body.should.have.property('message').eql('Mật khẩu đã được đặt lại thành công');
                        res.body.should.have.property('data').eql([]);
                        done();
                    }
                });
        });
    });

    const passportMock = {
        authenticate: (strategy, options, callback) => {
            if (callback) {
                return (req, res, next) => {
                    req.user = { id: 'testUserId' };
                    callback(null, req.user);
                    next();
                };
            }
            return (req, res, next) => {
                req.user = { id: 'testUserId' };
                next();
            };
        }
    };

    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('/GET google', () => {
        it('it should initiate Google login', (done) => {
            chai.request(server)
                .get('/api/auth/google')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    describe('/GET facebook', () => {
        it('it should initiate Facebook login', (done) => {
            chai.request(server)
                .get('/api/auth/facebook')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
});
