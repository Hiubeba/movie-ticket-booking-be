import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.mjs';
import Branch from '../models/Branch.mjs';
import User from '../models/User.mjs';
import jwt from 'jsonwebtoken';
import { connectTestDB, closeTestDB } from '../config/testDB.js';
import dotenv from 'dotenv';

dotenv.config();

const { expect } = chai;
chai.use(chaiHttp);

let adminToken;
let branchId;

describe('Branch API', () => {
    before(async () => {
        await connectTestDB();

        // Tạo admin user và lấy token
        const adminUser = new User({
            username: 'admin',
            password: 'adminpassword',
            role: 'admin',
            email: 'admin@example.com',
            fullName: 'Admin User'
        });
        await adminUser.save();

        adminToken = jwt.sign({ user: { id: adminUser._id, role: adminUser.role } }, process.env.JWT_SECRET, { expiresIn: '1h' });

        // Tạo một chi nhánh để test
        const branch = new Branch({
            name: 'Test Branch',
            country: 'Test Country',
            address: 'Test Address'
        });
        const savedBranch = await branch.save();
        branchId = savedBranch._id;
    });

    after(async () => {
        await User.deleteMany({});
        await Branch.deleteMany({});
        await closeTestDB();
    });

    it('should create a new branch', (done) => {
        chai.request(app)
            .post('/api/admin/branch')
            .set('Cookie', `token=${adminToken}`)
            .send({
                name: 'New Branch',
                country: 'New Country',
                address: 'New Address'
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                done();
            });
    });

    it('should update a branch', (done) => {
        chai.request(app)
            .put(`/api/admin/branch/${branchId}`)
            .set('Cookie', `token=${adminToken}`)
            .send({
                name: 'Updated Branch',
                country: 'Updated Country',
                address: 'Updated Address'
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                expect(res.body.data).to.have.property('name', 'Updated Branch');
                expect(res.body.data).to.have.property('country', 'Updated Country');
                expect(res.body.data).to.have.property('address', 'Updated Address');
                done();
            });
    });



    it('should get a branch by id', (done) => {
        chai.request(app)
            .get(`/api/admin/branch/${branchId}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                done();
            });
    });

    it('should get all branches', (done) => {
        chai.request(app)
            .get('/api/admin/branch')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data').that.is.an('array');
                done();
            });
    });

    it('should delete a branch', (done) => {
        chai.request(app)
            .delete(`/api/admin/branch/${branchId}`)
            .set('Cookie', `token=${adminToken}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('message', 'Đã xóa rạp phim');
                done();
            });
    });
});
