import chai from 'chai';
import chaiHttp from 'chai-http';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import app from '../server.mjs';
import Movie from '../models/Movie.mjs';
import User from '../models/User.mjs';
import jwt from 'jsonwebtoken';
import { connectTestDB, closeTestDB } from '../config/testDB.js';
import dotenv from 'dotenv';

dotenv.config();

const { expect } = chai;
chai.use(chaiHttp);

// Định nghĩa __dirname
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let adminToken;
let movieId;

describe('Movie API', () => {
    before(async () => {
        // Kết nối đến MongoDB test database
        await connectTestDB();

        // Tạo admin user và lấy token
        const adminUser = new User({
            username: 'admin',
            password: 'adminpassword',
            role: 'admin',
            email: 'admin@example.com',
            fullName: 'Admin User'
        });
        await adminUser.save();

        adminToken = jwt.sign({ user: { id: adminUser._id } }, process.env.JWT_SECRET, { expiresIn: '1h' });
    });

    after(async () => {
        // Xóa dữ liệu sau khi test
        await User.deleteMany({});
        await Movie.deleteMany({});
        await closeTestDB();
    });

    it('should create a new movie', (done) => {
        chai.request(app)
            .post('/api/admin')
            .set('Cookie', `token=${adminToken}`)
            .field('movieName', 'Test Movie')
            .field('category', 'Action')
            .field('time', 120)
            .field('startDay', '2023-12-12')
            .attach('banner', fs.readFileSync(path.join(__dirname, 'test-image.jpg')), 'test-image.jpg')
            .end((err, res) => {
                expect(res).to.have.status(201);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                movieId = res.body.data._id;
                done();
            });
    });

    it('should update a movie', (done) => {
        chai.request(app)
            .put(`/api/admin/${movieId}`)
            .set('Cookie', `token=${adminToken}`)
            .field('movieName', 'Updated Test Movie')
            .field('category', 'Adventure')
            .attach('banner', fs.readFileSync(path.join(__dirname, 'test-image.jpg')), 'test-image.jpg')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                expect(res.body.data).to.have.property('movieName', 'Updated Test Movie');
                done();
            });
    });

    it('should get all movies', (done) => {
        chai.request(app)
            .get('/api/admin')
            .set('Cookie', `token=${adminToken}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                expect(res.body.data).to.be.an('array');
                done();
            });
    });

    it('should delete a movie', (done) => {
        chai.request(app)
            .delete(`/api/admin/${movieId}`)
            .set('Cookie', `token=${adminToken}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('message', 'Xóa thành công');
                done();
            });
    });
});
