import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.mjs';
import Movie from '../models/Movie.mjs';
import MovieDetail from '../models/MovieDetail.mjs';
import User from '../models/User.mjs';
import jwt from 'jsonwebtoken';
import { connectTestDB, closeTestDB } from '../config/testDB.js';
import dotenv from 'dotenv';

dotenv.config();

const { expect } = chai;
chai.use(chaiHttp);

let adminToken;
let movieId;
let detailId;

describe('MovieDetail API', () => {
    before(async () => {
        await connectTestDB();

        // Tạo admin user và lấy token
        const adminUser = new User({
            username: 'admin',
            password: 'adminpassword',
            role: 'admin',
            email: 'admin@example.com',
            fullName: 'Admin User'
        });
        await adminUser.save();

        adminToken = jwt.sign({ user: { id: adminUser._id, role: adminUser.role } }, process.env.JWT_SECRET, { expiresIn: '1h' });

        // Tạo movie để lấy movieId
        const movie = new Movie({
            movieName: 'Test Movie',
            banner: 'test-banner-path',
            category: 'Action',
            time: 120,
            startDay: '2023-12-12'
        });
        const savedMovie = await movie.save();
        movieId = savedMovie._id;
    });

    after(async () => {
        await User.deleteMany({});
        await Movie.deleteMany({});
        await MovieDetail.deleteMany({});
        await closeTestDB();
    });

    it('should create a new movie detail', (done) => {
        chai.request(app)
            .post('/api/admin/movieDetail')
            .set('Cookie', `token=${adminToken}`)
            .send({
                movie: movieId,
                description: 'Test Description',
                director: 'Test Director',
                cast: ['Actor 1', 'Actor 2'],
                trailerUrl: 'http://example.com/trailer',
                releaseDate: '2023-12-12'
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                detailId = res.body.data._id;
                done();
            });
    });

    it('should update a movie detail', (done) => {
        chai.request(app)
            .put(`/api/admin/movieDetail/${detailId}`)
            .set('Cookie', `token=${adminToken}`)
            .send({
                description: 'Updated Description',
                director: 'Updated Director'
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                expect(res.body.data).to.have.property('description', 'Updated Description');
                expect(res.body.data).to.have.property('director').that.includes('Updated Director');
                done();
            });
    });

    it('should get a movie detail by movie id', (done) => {
        chai.request(app)
            .get(`/api/admin/movieDetail/${movieId}`)
            .set('Cookie', `token=${adminToken}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                expect(res.body.data).to.have.property('description', 'Updated Description');
                expect(res.body.data).to.have.property('director').that.includes('Updated Director');
                done();
            });
    });

    it('should delete a movie detail', (done) => {
        chai.request(app)
            .delete(`/api/admin/movieDetail/${detailId}`)
            .set('Cookie', `token=${adminToken}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('message', 'Thành công');
                done();
            });
    });
});
